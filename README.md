# Carnatic Music

Deep learning for Carnatic music. Objectives:
• develop neural net architecture for melody (Raagam) identification
• generate music through deep learning recurrent neural network (RNN)